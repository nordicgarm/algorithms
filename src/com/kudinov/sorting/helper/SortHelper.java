package com.kudinov.sorting.helper;

/**
 * Created by dkudinov on 07.04.2017.
 */
public class SortHelper {
    public static void checkSortedArray(int[] arr) throws ArrayNonSortedException {
        // Check array on correct sorting
        int err = 0;
        for (int i = 0; i < arr.length - 2 && err == 0; i++) {
            if (arr[i] > arr[i + 1]) {
                err = i;
            }
        }

        if (err != 0) {
            throw new ArrayNonSortedException(err);
        }
    }

    public static int[] getRndArr(int n) {
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = (int) (Math.random() * n * 10);
        }

        return arr;
    }

    public static String nLogn(int n) {
        return String.format("Complexity n * log n = %s (n = %s)", (n * (int) (Math.log(n) / Math.log(2) + 1)), n);
    }

    public static String n2(int n) {
        return String.format("Complexity n^2 = %s (n = %s)", (n * n), n);
    }
}
