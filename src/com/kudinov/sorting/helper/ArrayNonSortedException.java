package com.kudinov.sorting.helper;
import java.util.*;

public class ArrayNonSortedException extends Throwable
{
	public ArrayNonSortedException() {}
	
	public ArrayNonSortedException(int[] arr) {
		System.out.println(Arrays.toString(arr));
	}
	
	public ArrayNonSortedException(String s) {
		System.out.println(s);
	}
	
	public ArrayNonSortedException(int errIndex) {
		System.out.println("error on index " + errIndex);
	}
}
