package com.kudinov.sorting.combsort;

/**
 * Created by dkudinov on 05.04.2017.
 */
public class CombSortEngine {
	private static int steps;
	private static final double SHRINK_FACTOR = 1.247;
	
	public static void combSort(int[] arr) {
		steps = 0;
		
		combSort0(arr);
	}
	
	private static void combSort0(int[] arr) {
		int gap = arr.length;
		boolean swapped = false;
			
		while (gap > 1 || swapped) {
			if (gap > 1) {
				gap = (int) (gap / SHRINK_FACTOR);
			}
				
			swapped = false;
			for (int left = 0, right = gap; right < arr.length; left++, right++) {
				if (arr[left] > arr[right]) {
					int tmp = arr[left];
					arr[left] = arr[right];
					arr[right] = tmp;
					swapped = true;
				}
				steps++;
			}
		}
	}
	
	public static int getSteps() {
		return steps;
	}
}
