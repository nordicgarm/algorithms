package com.kudinov.sorting.combsort;

import com.kudinov.sorting.helper.*;
import java.util.*;

/**
 * Created by dkudinov on 05.04.2017.
 */
public class CombSort {
	public static void main(String[] args) {
		int n = 1000001;
		System.out.println(SortHelper.nLogn(n));
		
		int[] arr = SortHelper.getRndArr(n);
		
		CombSortEngine.combSort(arr);

		try {
			SortHelper.checkSortedArray(arr);
			System.out.println(CombSortEngine.getSteps());
		} catch (ArrayNonSortedException e) {
			System.out.println(Arrays.toString(arr));
			e.printStackTrace();
		}
	}
}
