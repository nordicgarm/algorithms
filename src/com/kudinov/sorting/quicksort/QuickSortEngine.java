package com.kudinov.sorting.quicksort;

public class QuickSortEngine
{
	private static int steps;
	private static int deep;
	private static boolean logging;

	public static void sort(int[] arr){
		steps = 0;

		System.out.println(String.format("Complexity n * log n = %s (n = %s)", arr.length * (int) (Math.log(arr.length) / Math.log(2) + 1), arr.length));

		sort0(arr, 0, arr.length - 1);

		System.out.println("steps " + steps);
		System.out.println("deep " + deep);
	}

	private static void sort0(int[] arr, int left, int right) {
		if (left < right) {
			int pivot = partition(arr, left, right);
			deep++;
			sort0(arr, left, pivot - 1);
			sort0(arr, pivot + 1, right);
		}
	}

	private static int partition(int[] arr, int left, int right) {
		int pivot = right + left >>> 1;
		int pivotVal = arr[pivot];
		swap(arr, pivot, right);
		int i = left;

		for (i = pivot = left; i < right; ++ i) {
			if (arr[i] <= pivotVal) {
				swap(arr, pivot++, i);
			}
			steps++;
		}
		swap(arr, pivot, right);
		return (pivot);
	}

	private static void swap(int[] arr, int i, int j) {
		int tmp = arr[i];
		arr[i] = arr[j];
		arr[j] = tmp;
	}

	private static void log(String text) {
		if (!logging) {
			return;
		}
		System.out.println(text);
	}

	public static int getSteps() {
		return steps;
	}

	public static int getDeep() {
		return deep;
	}
}
