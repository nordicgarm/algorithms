package com.kudinov.sorting.quicksort;

import com.kudinov.sorting.helper.*;
import java.util.Arrays;

public class QuickSort
{
	public static void main(String[] args) {
		int n = 10;
		if (args.length != 0) {
			n = Integer.parseInt(args[0]);
		}

		int[] arr = SortHelper.getRndArr(n);

		QuickSortEngine.sort(arr);

		try {
			SortHelper.checkSortedArray(arr);
			System.out.println(QuickSortEngine.getSteps());
		} catch(ArrayNonSortedException e) {
			System.out.println(Arrays.toString(arr));
			e.printStackTrace();
		}
	}
}
