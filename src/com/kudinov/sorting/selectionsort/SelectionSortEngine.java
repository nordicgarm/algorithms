package com.kudinov.sorting.selectionsort;

public class SelectionSortEngine
{
	private static int steps;

	public static void sort(int[] arr) {
		steps = 0;

		for (int i = 0; i < arr.length-1; i++) {
			for (int j = i; j < arr.length; j++) {
				if (arr[j] < arr[i]) {
					int tmp = arr[i];
					arr[i] = arr[j];
					arr[j] = tmp;
				}
				steps++;
			}
		}
	}

	/*
	* StackOverflow warning.
	*/
	public static void sortRecursive(int[] arr) {
		steps = 0;
		
		sortRecursive0(arr, 0);
	}
	
	private static void sortRecursive0(int[] arr, int index) {
		if (index == arr.length) {
			return;
		} else {
			for (int i = index; i < arr.length; i++) {
				if (arr[i] < arr[index]) {
					int temp = arr[i];
					arr[i] = arr[index];
					arr[index] = temp;
				}
				steps++;
			}
			
			sortRecursive0(arr, ++index);
		}
	}
	
	public static int getSteps() {
		return steps;
	}
}
