package com.kudinov.sorting.selectionsort;

import com.kudinov.sorting.helper.ArrayNonSortedException;
import com.kudinov.sorting.helper.SortHelper;

import java.util.*;

public class SelectionSort
{
	public static void main(String[] args) {
		int n = 100;

		int[] arr = SortHelper.getRndArr(n);
		int[] arr2 = new int[n];
		System.arraycopy(arr, 0, arr2, 0, arr.length);
		
		System.out.println("init arr: " + Arrays.toString(arr));
		SelectionSortEngine.sort(arr);

		try {
			SortHelper.checkSortedArray(arr);
			System.out.println(SelectionSortEngine.getSteps());
		} catch(ArrayNonSortedException e) {
			System.out.println(Arrays.toString(arr));
			e.printStackTrace();
		}
		
		System.out.println("with recursive");
		SelectionSortEngine.sortRecursive(arr2);
		try {
			SortHelper.checkSortedArray(arr2);
			System.out.println(SelectionSortEngine.getSteps());
		} catch(ArrayNonSortedException e) {
			System.out.println(Arrays.toString(arr2));
			e.printStackTrace();
		}
	}
} 
