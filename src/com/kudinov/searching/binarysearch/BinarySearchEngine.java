package com.kudinov.searching.binarysearch;

public class BinarySearchEngine {
    Number answer;
    int steps;
    int[] arr;

    public BinarySearchEngine(Number n, int[] arr) {
        answer = n;
        this.arr = arr;
    }

	public int searchRecursive() {
		steps = 0;
		
		return searchRecursive0(0, arr.length);
	}
	
	private int searchRecursive0(int low, int high) {
		steps++;
		int mid = low + high >>> 1;
		int midVal = arr[mid];
		//System.out.println("low " + low + " high " + high + " mid " + mid);
		
		if (low > high) {
			return -(low + 1);
		}
		if (answer.isAnswer(midVal)) {
			return mid;
		} else if (answer.isLesser(midVal)) {
			mid = searchRecursive0(0, mid - 1);
		} else if (answer.isBigger(midVal)) {
			mid = searchRecursive0(mid + 1, high);
		}
		
		return mid;
	}
	
    public int search() {
		steps = 0;
		
		return search0(0, arr.length);
    }

	private int search0(int low, int high) {
		high--;
		
		while (low <= high) {
			steps++;
			int mid = (low + high) >>> 1;
			int midVal = arr[mid];
			
			if (answer.isAnswer(midVal)) {
				return mid;
			} else if (answer.isLesser(midVal)) {
				high = mid - 1;
			} else if (answer.isBigger(midVal)) {
				low = mid + 1;
			}
		}
		
		return -(low + 1);
	}
	
    public int getSteps() {
        return steps;
    }
}
