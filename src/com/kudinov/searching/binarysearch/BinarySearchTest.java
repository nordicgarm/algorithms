package com.kudinov.searching.binarysearch;

public class BinarySearchTest {
    public static void main(String[] args) {
        int answer = 73;
        int[] list = new int[10000000];
        for (int i = 0; i < 10000000; i++)
            list[i] = i;
		
        BinarySearchEngine test = new BinarySearchEngine(new Number(answer), list);

        System.out.println("answer is " + test.search() + " (right answer is " + answer + "). Steps: " + test.getSteps());
		System.out.println("answer is " + test.searchRecursive() + " (right answer is " + answer + "). Steps: " + test.getSteps());
    }
}
