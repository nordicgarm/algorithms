package com.kudinov.searching.binarysearch;

public class Test
{
	public static void main(String[] args) {
		int a = 100;
		System.out.println(a);
		a = a >> 1;
		System.out.println("100 >> " + a);
		a = 100;
		a = a >>> 1;
		System.out.println("100 >>> " + a);
		a = -100;
		a = a >> 1;
		System.out.println("-100 >> " + a);
		a = -100;
	}
}
