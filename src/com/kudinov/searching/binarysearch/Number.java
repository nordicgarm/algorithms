package com.kudinov.searching.binarysearch;

public class Number {
    private int num;

    public Number(int n) {
        num = n;
    };

    public void setNum(int n) {
        num = n;
    }

    public boolean isAnswer(int n) {
        return (n == num);
    }

    public boolean isBigger(int n) {
        return (n < num);
    }

    public boolean isLesser(int n) {
        return (n > num);
    }
}
