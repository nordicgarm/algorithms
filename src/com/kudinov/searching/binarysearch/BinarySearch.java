package com.kudinov.searching.binarysearch;

import java.util.Scanner;

/**
 * Created by garm on 12.03.2017.
 */
public class BinarySearch {
    public static void main(String[] args) {
        BinarySearch binarySearch = new BinarySearch();
        binarySearch.go();
    }

    public void go() {
        int max = 100;
        int num = 0;
        int[] range = new int[max];
        for (int i = 0; i < max; i++)
            range[i] = i + 1;

        System.out.println("Enter number in range from 1 to " + max);
        Scanner scanner = new Scanner(System.in);

        num = scanner.nextInt();

        BinarySearchEngine binarySearchEngine = new BinarySearchEngine(new Number(num), range);

        int result = binarySearchEngine.search();

        if (result == -1)
            System.out.println(String.format("The number %s is not found in range", num));
        else
            System.out.println("Answer is " + range[result] + " (right answer is " + num + "). Steps: " + binarySearchEngine.getSteps());
    }
}
