package com.kudinov.recursions;

public class RecursionSum
{
	public static void main(String[] args) {
		int[] arr = {9};
		RecursionSum app = new RecursionSum();
		System.out.println(app.sum(arr));
		System.out.println(app.sumRecursive(arr));
	}
	
	public int sum(int[] arr) {
		int sum = 0;
		for (int i = 0; i < arr.length; i++) {
			sum += arr[i];
		}
		return sum;
	}
	
	public int sumRecursive(int[] arr) {
		return sumRecursive0(arr, 0);
	}
	
	private int sumRecursive0(int[] arr, int index) {
		if (arr.length <= index) {
			return 0;
		} else if (arr.length == index - 1) {
			return arr[index];
		} else {
			return arr[index] + sumRecursive0(arr, ++index);
		}
	}
}
