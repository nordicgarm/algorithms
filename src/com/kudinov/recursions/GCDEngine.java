package com.kudinov.recursions;

public class GCDEngine
{
	int steps;
	
	public int gcd(int a, int b) {
		steps = 0;
		
		while (b != 0) {
			int tmp = a % b;
			a = b;
			b = tmp;
			steps++;
		}

		return a;
	}

	public int gcdRecursive(int a, int b) {
		steps = 0;
		return gcdRecursive0(a, b);
	}
	
	private int gcdRecursive0(int a, int b) {
		steps++;
		return b == 0 ? a : gcdRecursive0(b, a % b);
	}
	
	public int getSteps() {
		return steps;
	}
}
