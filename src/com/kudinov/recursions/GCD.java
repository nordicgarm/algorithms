package com.kudinov.recursions;

public class GCD
{
	public static void main(String[] args) {
		int a = 1680;
		int b = 640;
		GCDEngine gcd = new GCDEngine();
		
		System.out.println(gcd.gcd(a, b));
		
		System.out.println(gcd.gcdRecursive(a, b));
	}
}
